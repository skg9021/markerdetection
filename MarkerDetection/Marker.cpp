#include "Marker.h"


Marker::Marker(void)
{
	id=-1;
	ssize=-1;
	Rvec.create(3,1,CV_32FC1);
	Tvec.create(3,1,CV_32FC1);
	for (int i=0;i<3;i++)
		Tvec.at<float>(i,0)=Rvec.at<float>(i,0)=-999999;
}


Marker::~Marker(void)
{
}

void Marker::calculateExtrinsics( float markerSizeMeters,cv::Mat camMatrix,cv::Mat distCoeff/*=cv::Mat()*/,bool setYPerperdicular/*=true*/ ) throw(cv::Exception)
{
	if (!isValid()) throw cv::Exception(9004,"!isValid(): invalid marker. It is not possible to calculate extrinsics","calculateExtrinsics",__FILE__,__LINE__);
	if (markerSizeMeters<=0)throw cv::Exception(9004,"markerSize<=0: invalid markerSize","calculateExtrinsics",__FILE__,__LINE__);
	if ( camMatrix.rows==0 || camMatrix.cols==0) throw cv::Exception(9004,"CameraMatrix is empty","calculateExtrinsics",__FILE__,__LINE__);

	double halfSize=markerSizeMeters/2.;
	cv::Mat ObjPoints(4,3,CV_32FC1);
	ObjPoints.at<float>(1,0)=-halfSize;
	ObjPoints.at<float>(1,1)=halfSize;
	ObjPoints.at<float>(1,2)=0;
	ObjPoints.at<float>(2,0)=halfSize;
	ObjPoints.at<float>(2,1)=halfSize;
	ObjPoints.at<float>(2,2)=0;
	ObjPoints.at<float>(3,0)=halfSize;
	ObjPoints.at<float>(3,1)=-halfSize;
	ObjPoints.at<float>(3,2)=0;
	ObjPoints.at<float>(0,0)=-halfSize;
	ObjPoints.at<float>(0,1)=-halfSize;
	ObjPoints.at<float>(0,2)=0;

	cv::Mat ImagePoints(4,2,CV_32FC1);

	//Set image points from the marker
	for (int c=0;c<4;c++)
	{
		ImagePoints.at<float>(c,0)=((*this)[c%4].x);
		ImagePoints.at<float>(c,1)=((*this)[c%4].y);
	}

	cv::Mat raux,taux;
	cv::solvePnP(ObjPoints, ImagePoints, camMatrix, distCoeff,raux,taux);
	raux.convertTo(Rvec,CV_32F);
	taux.convertTo(Tvec ,CV_32F);
	//cout<<"Rot:"<<Rvec<<endl;
	//cout<<"Tran:"<<Tvec<<endl;
	//rotate the X axis so that Y is perpendicular to the marker plane
	if (setYPerperdicular) rotateXAxis(Rvec);
	ssize=markerSizeMeters; 
	//cout<<(*this)<<endl;
}

void Marker::glGetModelViewMatrix( double modelview_matrix[16] ) throw(cv::Exception)
{
	//check if paremeters are valid
	bool invalid=false;
	for (int i=0;i<3 && !invalid ;i++)
	{
		if (Tvec.at<float>(i,0)!=-999999) invalid|=false;
		if (Rvec.at<float>(i,0)!=-999999) invalid|=false;
	}
	if (invalid) throw cv::Exception(9003,"extrinsic parameters are not set","Marker::getModelViewMatrix",__FILE__,__LINE__);
	Mat Rot(3,3,CV_32FC1),Jacob;
	cv::Rodrigues(Rvec, Rot, Jacob);

	double para[3][4];
	for (int i=0;i<3;i++)
		for (int j=0;j<3;j++) para[i][j]=Rot.at<float>(i,j);
	//now, add the translation
	para[0][3]=Tvec.at<float>(0,0);
	para[1][3]=Tvec.at<float>(1,0);
	para[2][3]=Tvec.at<float>(2,0);
	double scale=1;

	modelview_matrix[0 + 0*4] = para[0][0];
	// R1C2
	modelview_matrix[0 + 1*4] = para[0][1];
	modelview_matrix[0 + 2*4] = para[0][2];
	modelview_matrix[0 + 3*4] = para[0][3];
	// R2
	modelview_matrix[1 + 0*4] = para[1][0];
	modelview_matrix[1 + 1*4] = para[1][1];
	modelview_matrix[1 + 2*4] = para[1][2];
	modelview_matrix[1 + 3*4] = para[1][3];
	// R3
	modelview_matrix[2 + 0*4] = -para[2][0];
	modelview_matrix[2 + 1*4] = -para[2][1];
	modelview_matrix[2 + 2*4] = -para[2][2];
	modelview_matrix[2 + 3*4] = -para[2][3];
	modelview_matrix[3 + 0*4] = 0.0;
	modelview_matrix[3 + 1*4] = 0.0;
	modelview_matrix[3 + 2*4] = 0.0;
	modelview_matrix[3 + 3*4] = 1.0;
	if (scale != 0.0)
	{
		modelview_matrix[12] *= scale;
		modelview_matrix[13] *= scale;
		modelview_matrix[14] *= scale;
	}
}

void Marker::rotateXAxis( cv::Mat &rotation )
{

}
