#include "MarkerDetect.h"


MarkerDetect::MarkerDetect(void)
{
	thresh = 220;
	N = 11;
	wndname = "Square Detection Demo";
	canny = "Canny";
}


MarkerDetect::~MarkerDetect(void)
{
}

void MarkerDetect::help()
{
	 cout <<
    "\nA program using pyramid scaling, Canny, contours, contour simpification and\n"
    "memory storage (it's got it all folks) to find\n"
    "squares in a list of images pic1-6.png\n"
    "Returns sequence of squares detected on the image.\n"
    "the sequence is stored in the specified memory storage\n"
    "Call:\n"
    "./squares\n"
    "Using OpenCV version %s\n" << CV_VERSION << "\n" << endl;
}

// helper function:
// finds a cosine of angle between vectors
// from pt0->pt1 and from pt0->pt2
double MarkerDetect::angle( Point pt1, Point pt2, Point pt0 )
{
	double dx1 = pt1.x - pt0.x;
    double dy1 = pt1.y - pt0.y;
    double dx2 = pt2.x - pt0.x;
    double dy2 = pt2.y - pt0.y;
    return (dx1*dx2 + dy1*dy2)/sqrt((dx1*dx1 + dy1*dy1)*(dx2*dx2 + dy2*dy2) + 1e-10);
}

void MarkerDetect::findSquares(const cv::Mat &input,std::vector<Marker> &detectedMarkers, CameraParams camParams,float markerSizeMeters,bool setYPerperdicular) throw (cv::Exception)
{
	findSquares ( input, detectedMarkers,camParams.CameraMatrix ,camParams.Distorsion,  markerSizeMeters ,setYPerperdicular);
}

// returns sequence of squares detected on the image.
// the sequence is stored in the specified memory storage
void MarkerDetect::findSquares( const Mat& image, vector<Marker>& squares,Mat camMatrix ,Mat distCoeff ,float markerSizeMeters ,bool setYPerperdicular )
{
	squares.clear();
	marker.clear();

    Mat pyr, timg, gray0(image.size(), CV_8U), gray;

    // down-scale and upscale the image to filter out the noise
    pyrDown(image, pyr, Size(image.cols/2, image.rows/2));
    pyrUp(pyr, timg, image.size());
    vector<vector<Point> > contours;
	vector<Vec4i> hierarchy;

    // find squares in every color plane of the image
    for( int c = 0; c < 3; c++ )
    {
        int ch[] = {c, 0};
        mixChannels(&timg, 1, &gray0, 1, ch, 1);

        // try several threshold levels
        for( int l = 0; l < N; l++ )
        {
            // hack: use Canny instead of zero threshold level.
            // Canny helps to catch squares with gradient shading
            if( l == 0 )
            {
                // apply Canny. Take the upper threshold from slider
                // and set the lower to 0 (which forces edges merging)
                Canny(gray0, gray, 0, thresh, 5);
                // dilate canny output to remove potential
                // holes between edge segments
                dilate(gray, gray, Mat(), Point(-1,-1));
                imshow( canny, gray);
            }
            else
            {
                // apply threshold if l!=0:
                //     tgray(x,y) = gray(x,y) < (l+1)*255/N ? 255 : 0
                gray = gray0 >= (l+1)*255/N;
            }
            
            
            
            // find contours and store them all as a list
            //findContours(gray, contours, CV_RETR_LIST, CV_CHAIN_APPROX_SIMPLE);
			findContours( gray, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE );
            //drawContours(cont, contours,-1,CV_RGB(255,0,0),2,8);
            
            
            vector<Point> approx;

            // test each contour
            for( size_t i = 0; i < contours.size(); i++ )
            {
                // approximate contour with accuracy proportional
                // to the contour perimeter
				//if(contours[i].size()<2000 && contours[i].size()>0)
				approxPolyDP(Mat(contours[i]), approx, arcLength(Mat(contours[i]), true)*0.02, true);

                // square contours should have 4 vertices after approximation
                // relatively large area (to filter out noisy contours)
                // and be convex.
                // Note: absolute value of an area is used because
                // area may be positive or negative - in accordance with the
                // contour orientation
                if( approx.size() == 4 &&
                    fabs(contourArea(Mat(approx))) > 500 && fabs(contourArea(Mat(approx))) < 2000 &&
                    isContourConvex(Mat(approx)) )
                {

                    double maxCosine = 0;

                    for( int j = 2; j < 5; j++ )
                    {
                        // find the maximum cosine of the angle between joint edges
                        double cosine = fabs(angle(approx[j%4], approx[j-2], approx[j-1]));
                        maxCosine = MAX(maxCosine, cosine);
                    }

                    // if cosines of all angles are small
                    // (all angles are ~90 degree) then write quandrange
                    // vertices to resultant sequence
                    if( maxCosine < 0.3 )
					{
                        //squares.push_back(approx);
						marker.push_back(Marker());
						marker.back().id = i;

						vector<Point> hullArray;
						convexHull( Mat(approx), hullArray, false );
						hull.push_back(hullArray);
						//squares.back().push_back(hullArray);
						
						for ( int j=0;j<4;j++ )
						{
							marker.back().push_back ( Point ( hullArray[j].x,hullArray[j].y ) );
						}
						

					}
                }
            }
            
        }
    }

	
	/// remove these elements while corners are too close to each other
	//first detect candidates

	vector<pair<int,int>  > TooNearCandidates;
	for ( unsigned int i=0;i<marker.size();i++ )
	{
		// 	cout<<"Marker i="<<i<<MarkerCanditates[i]<<endl;
		//calculate the average distance of each corner to the nearest corner of the other marker candidate
		for ( unsigned int j=i+1;j<marker.size();j++ )
		{
			float dist=0;
			for ( int c=0;c<4;c++ )
				dist+= sqrt ((float)( ( marker[i][c].x-marker[j][c].x ) * ( marker[i][c].x-marker[j][c].x ) + ( marker[i][c].y-marker[j][c].y ) * ( marker[i][c].y-marker[j][c].y ) ) );
			dist/=4;
			//if distance is too small
			if ( dist< 10 )
			{
				TooNearCandidates.push_back ( pair<int,int> ( i,j ) );
			}
		}
	}

	
	//mark for removal the element of  the pair with smaller perimeter
	valarray<bool> toRemove ( false,marker.size() );
	for ( unsigned int i=0;i<TooNearCandidates.size();i++ )
	{
		if ( perimeter ( marker[TooNearCandidates[i].first ] ) >perimeter ( marker[ TooNearCandidates[i].second] ) )
			toRemove[TooNearCandidates[i].second]=true;
		else toRemove[TooNearCandidates[i].first]=true;
	}

	
	//remove the invalid ones
	//     removeElements ( MarkerCanditates,toRemove );
	//finally, assign to the remaining candidates the contour
	squares.reserve(marker.size());
	for (size_t i=0;i<marker.size();i++) {
		if (!toRemove[i]) {
			squares.push_back(marker[i]);
			//OutMarker.back().contour=contours2[ marker[i].idx];
			//if (swapped[i] && _enableCylinderWarp )//if the corners where swapped, it is required to reverse here the points so that they are in the same order
				//reverse(OutMarker.back().contour.begin(),OutMarker.back().contour.end());//????
		}
	}

	///detect the position of detected markers if desired
	if ( camMatrix.rows!=0  && markerSizeMeters>0 )
	{
		for ( unsigned int i=0;i<squares.size();i++ )
			squares[i].calculateExtrinsics ( markerSizeMeters,camMatrix,distCoeff,setYPerperdicular );
	}
	
	
}

// the function draws all the squares in the image
void MarkerDetect::drawSquares( Mat& image, const vector<Marker>& squares )
{
	//vector<vector<Point> >hull(squares.size());

	for( size_t i = 0; i < squares.size(); i++ )
    {
		/*
		convexHull( Mat(squares[i]), hull[i], false );
		for(int j=0;j<hull[i].size();j++) {
			Point p = hull[i][j];
			circle(image,p,5,CV_RGB(255,0,0),-1);
		}
		*/
		
		//const Point *p = &squares[i][0];
        //int n = (int)squares[i].size();
        //polylines(image, &p, &n, 1, true, Scalar(0,255,0), 1, CV_AA);
		//polylines(image, squares[i], true, Scalar(0,255,0),1,CV_AA,0);
		polylines(image, squares[i], true, Scalar(0,255,0), 1, CV_AA, 0);
    }
	hull.clear();
	
	imshow(wndname, image);
}

int MarkerDetect::perimeter( std::vector<cv::Point> &a )
{
	int sum=0;
	for ( unsigned int i=0;i<a.size();i++ )
	{
		int i2= ( i+1 ) %a.size();
		sum+= sqrt ( (float)( ( a[i].x-a[i2].x ) * ( a[i].x-a[i2].x ) + ( a[i].y-a[i2].y ) * ( a[i].y-a[i2].y ) ) );
	}
	return sum;
}

