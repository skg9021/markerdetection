#pragma once

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include "Marker.h"
#include "CameraParams.h"

using namespace cv;

class MarkerDraw
{
public:
	MarkerDraw(void);
	~MarkerDraw(void);

	static void draw3dAxis(cv::Mat &Image,Marker &m,const CameraParams &CP);

	static void draw3dCube(cv::Mat &Image,Marker &m,const CameraParams &CP);
};

