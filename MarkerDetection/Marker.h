#pragma once

#include <vector>
#include <iostream>
#include <opencv2\core\core.hpp>
#include <opencv2\calib3d\calib3d.hpp>

using namespace cv;
using namespace std;

class Marker : public std::vector<cv::Point>
{
public:
	//id of  the marker
	int id;
	//size of the markers sides in meters
	float ssize;
	//matrices of rotation and translation respect to the camera
	cv::Mat Rvec,Tvec;

	Marker(void);
	~Marker(void);

	/**Indicates if this object is valid
     */
    bool isValid()const{return id!=-1 && size()==4;}

    /**Calculates the extrinsics (Rvec and Tvec) of the marker with respect to the camera
     * @param markerSize size of the marker side expressed in meters
     * @param CameraMatrix matrix with camera parameters (fx,fy,cx,cy)
     * @param Distorsion matrix with distorsion parameters (k1,k2,p1,p2)
     * @param setYPerperdicular If set the Y axis will be perpendicular to the surface. Otherwise, it will be the Z axis
     */
    void calculateExtrinsics(float markerSizeMeters,cv::Mat  camMatrix,cv::Mat distCoeff=cv::Mat(),bool setYPerperdicular=true)throw(cv::Exception);
    
    /**Given the extrinsic camera parameters returns the GL_MODELVIEW matrix for opengl.
     * Setting this matrix, the reference coordinate system will be set in this marker
     */
    void glGetModelViewMatrix(  double modelview_matrix[16])throw(cv::Exception);

private:
	
	void rotateXAxis(cv::Mat &rotation);
};

