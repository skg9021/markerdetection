#include "CameraParams.h"


CameraParams::CameraParams(void)
{
	CameraMatrix=cv::Mat();
	Distorsion=cv::Mat();
	CamSize=cv::Size(-1,-1);
}


CameraParams::~CameraParams(void)
{
}

void CameraParams::readFromXMLFile( string filePath ) throw(cv::Exception)
{
	cv::FileStorage fs(filePath, cv::FileStorage::READ);
	int w=-1,h=-1;
	cv::Mat MCamera,MDist;

	MCamera = cv::Mat(3,3,CV_32FC1);

	fs["image_width"] >> w;
	fs["image_height"] >> h;
	fs["distortion_coefficients"] >> MDist;
	fs["camera_matrix"] >> MCamera;

	if (MCamera.cols==0 || MCamera.rows==0)throw cv::Exception(9007,"File :"+filePath+" does not contains valid camera matrix","CameraParameters::readFromXML",__FILE__,__LINE__);
	if (w==-1 || h==0)throw cv::Exception(9007,"File :"+filePath+" does not contains valid camera dimensions","CameraParameters::readFromXML",__FILE__,__LINE__);

	std::cout<<MCamera<<std::endl;
	std::cout<<MDist<<std::endl;

	if (MCamera.type()!=CV_32FC1)
		MCamera.convertTo(CameraMatrix,CV_32FC1);
	else
		CameraMatrix=MCamera;

	if (MDist.total()<4) throw cv::Exception(9007,"File :"+filePath+" does not contains valid distortion_coefficients","CameraParameters::readFromXML",__FILE__,__LINE__);
	//convert to 32 and get the 4 first elements only
	cv::Mat mdist32;
	MDist.convertTo(mdist32,CV_32FC1);
	Distorsion.create(1,4,CV_32FC1);
	for (int i=0;i<4;i++)
		Distorsion.ptr<float>(0)[i]=mdist32.ptr<float>(0)[i];

	CamSize.width=w;
	CamSize.height=h;
}

void CameraParams::glGetProjectionMatrix( cv::Size orgImgSize, cv::Size size,double proj_matrix[16],double gnear,double gfar,bool invert/*=false */ ) throw(cv::Exception)
{
	if (cv::countNonZero(Distorsion)!=0) std::cerr<< "CameraParameters::glGetProjectionMatrix :: The camera has distortion coefficients " <<__FILE__<<" "<<__LINE__<<endl;
	if (isValid()==false) throw cv::Exception(9100,"invalid camera parameters","CameraParameters::glGetProjectionMatrix",__FILE__,__LINE__);

	//Determine the resized info
	double Ax=double(size.width)/double(orgImgSize.width);
	double Ay=double(size.height)/double(orgImgSize.height);
	double _fx=CameraMatrix.at<float>(0,0)*Ax;
	double _cx=CameraMatrix.at<float>(0,2)*Ax;
	double _fy=CameraMatrix.at<float>(1,1)*Ay;
	double _cy=CameraMatrix.at<float>(1,2)*Ay;
	double cparam[3][4] =
	{
		{
			_fx,  0,  _cx,  0
		},
		{0,          _fy,  _cy, 0},
			{0,      0,      1,      0}
	};

	argConvGLcpara2( cparam, size.width, size.height, gnear, gfar, proj_matrix, invert );
}

void CameraParams::argConvGLcpara2( double cparam[3][4], int width, int height, double gnear, double gfar, double m[16], bool invert ) throw(cv::Exception)
{
	double   icpara[3][4];
	double   trans[3][4];
	double   p[3][3], q[4][4];
	int      i, j;

	cparam[0][2] *= -1.0;
	cparam[1][2] *= -1.0;
	cparam[2][2] *= -1.0;

	if ( arParamDecompMat(cparam, icpara, trans) < 0 )
		throw cv::Exception(9002,"parameter error","MarkerDetector::argConvGLcpara2",__FILE__,__LINE__);

	for ( i = 0; i < 3; i++ )
	{
		for ( j = 0; j < 3; j++ )
		{
			p[i][j] = icpara[i][j] / icpara[2][2];
		}
	}
	q[0][0] = (2.0 * p[0][0] / width);
	q[0][1] = (2.0 * p[0][1] / width);
	q[0][2] = ((2.0 * p[0][2] / width)  - 1.0);
	q[0][3] = 0.0;

	q[1][0] = 0.0;
	q[1][1] = (2.0 * p[1][1] / height);
	q[1][2] = ((2.0 * p[1][2] / height) - 1.0);
	q[1][3] = 0.0;

	q[2][0] = 0.0;
	q[2][1] = 0.0;
	q[2][2] = (gfar + gnear)/(gfar - gnear);
	q[2][3] = -2.0 * gfar * gnear / (gfar - gnear);

	q[3][0] = 0.0;
	q[3][1] = 0.0;
	q[3][2] = 1.0;
	q[3][3] = 0.0;

	for ( i = 0; i < 4; i++ )
	{
		for ( j = 0; j < 3; j++ )
		{
			m[i+j*4] = q[i][0] * trans[0][j]
			+ q[i][1] * trans[1][j]
			+ q[i][2] * trans[2][j];
		}
		m[i+3*4] = q[i][0] * trans[0][3]
		+ q[i][1] * trans[1][3]
		+ q[i][2] * trans[2][3]
		+ q[i][3];
	}

	if (!invert)
	{
		m[13]=-m[13] ;
		m[1]=-m[1];
		m[5]=-m[5];
		m[9]=-m[9];
	}
}

int CameraParams::arParamDecompMat( double source[3][4], double cpara[3][4], double trans[3][4] ) throw(cv::Exception)
{
	int       r, c;
	double    Cpara[3][4];
	double    rem1, rem2, rem3;

	if ( source[2][3] >= 0 )
	{
		for ( r = 0; r < 3; r++ )
		{
			for ( c = 0; c < 4; c++ )
			{
				Cpara[r][c] = source[r][c];
			}
		}
	}
	else
	{
		for ( r = 0; r < 3; r++ )
		{
			for ( c = 0; c < 4; c++ )
			{
				Cpara[r][c] = -(source[r][c]);
			}
		}
	}

	for ( r = 0; r < 3; r++ )
	{
		for ( c = 0; c < 4; c++ )
		{
			cpara[r][c] = 0.0;
		}
	}
	cpara[2][2] = norm( Cpara[2][0], Cpara[2][1], Cpara[2][2] );
	trans[2][0] = Cpara[2][0] / cpara[2][2];
	trans[2][1] = Cpara[2][1] / cpara[2][2];
	trans[2][2] = Cpara[2][2] / cpara[2][2];
	trans[2][3] = Cpara[2][3] / cpara[2][2];

	cpara[1][2] = dot( trans[2][0], trans[2][1], trans[2][2],
		Cpara[1][0], Cpara[1][1], Cpara[1][2] );
	rem1 = Cpara[1][0] - cpara[1][2] * trans[2][0];
	rem2 = Cpara[1][1] - cpara[1][2] * trans[2][1];
	rem3 = Cpara[1][2] - cpara[1][2] * trans[2][2];
	cpara[1][1] = norm( rem1, rem2, rem3 );
	trans[1][0] = rem1 / cpara[1][1];
	trans[1][1] = rem2 / cpara[1][1];
	trans[1][2] = rem3 / cpara[1][1];

	cpara[0][2] = dot( trans[2][0], trans[2][1], trans[2][2],
		Cpara[0][0], Cpara[0][1], Cpara[0][2] );
	cpara[0][1] = dot( trans[1][0], trans[1][1], trans[1][2],
		Cpara[0][0], Cpara[0][1], Cpara[0][2] );
	rem1 = Cpara[0][0] - cpara[0][1]*trans[1][0] - cpara[0][2]*trans[2][0];
	rem2 = Cpara[0][1] - cpara[0][1]*trans[1][1] - cpara[0][2]*trans[2][1];
	rem3 = Cpara[0][2] - cpara[0][1]*trans[1][2] - cpara[0][2]*trans[2][2];
	cpara[0][0] = norm( rem1, rem2, rem3 );
	trans[0][0] = rem1 / cpara[0][0];
	trans[0][1] = rem2 / cpara[0][0];
	trans[0][2] = rem3 / cpara[0][0];

	trans[1][3] = (Cpara[1][3] - cpara[1][2]*trans[2][3]) / cpara[1][1];
	trans[0][3] = (Cpara[0][3] - cpara[0][1]*trans[1][3]
	- cpara[0][2]*trans[2][3]) / cpara[0][0];

	for ( r = 0; r < 3; r++ )
	{
		for ( c = 0; c < 3; c++ )
		{
			cpara[r][c] /= cpara[2][2];
		}
	}

	return 0;
}

double CameraParams::norm( double a, double b, double c )
{
    return( sqrt( a*a + b*b + c*c ) );
}

double CameraParams::dot( double a1, double a2, double a3,
                              double b1, double b2, double b3 )
{
    return( a1 * b1 + a2 * b2 + a3 * b3 );
}

void CameraParams::resize( cv::Size size ) throw(cv::Exception)
{
	if (!isValid())  throw cv::Exception(9007,"invalid object","CameraParameters::resize",__FILE__,__LINE__);
	if (size==CamSize) return;
	//now, read the camera size
	//resize the camera parameters to fit this image size
	float AxFactor= float(size.width)/ float(CamSize.width);
	float AyFactor= float(size.height)/ float(CamSize.height);
	CameraMatrix.at<float>(0,0)*=AxFactor;
	CameraMatrix.at<float>(0,2)*=AxFactor;
	CameraMatrix.at<float>(1,1)*=AyFactor;
	CameraMatrix.at<float>(1,2)*=AyFactor;
}
