#pragma once

#include <opencv2/core/core.hpp>

#include <iostream>
#include <string>

using namespace cv;
using namespace std;

class CameraParams
{
public:
	CameraParams(void);
	~CameraParams(void);

	// 3x3 matrix (fx 0 cx, 0 fy cy, 0 0 1)
	cv::Mat  CameraMatrix;
	//4x1 matrix (k1,k2,p1,p2)
	cv::Mat  Distorsion;
	//size of the image
	cv::Size CamSize;

	/**Indicates whether this object is valid
     */
    bool isValid()const {
        return CameraMatrix.rows!=0 && CameraMatrix.cols!=0  && Distorsion.rows!=0 && Distorsion.cols!=0 && CamSize.width!=-1 && CamSize.height!=-1;
    }

	/**Reads from a YAML file generated with the opencv2.2 calibration utility
     */
    void readFromXMLFile(string filePath)throw(cv::Exception);

	/**Given the intrinsic camera parameters returns the GL_PROJECTION matrix for opengl.
    * PLease NOTE that when using OpenGL, it is assumed no camera distorsion! So, if it is not true, you should have
    * undistor image
    *
    * @param orgImgSize size of the original image
    * @param size of the image/window where to render (can be different from the real camera image). Please not that it must be related to CamMatrix
    * @param proj_matrix output projection matrix to give to opengl
    * @param gnear,gfar: visible rendering range
    * @param invert: indicates if the output projection matrix has to yield a horizontally inverted image because image data has not been stored in the order of glDrawPixels: bottom-to-top.
    */
    void glGetProjectionMatrix( cv::Size orgImgSize, cv::Size size,double proj_matrix[16],double gnear,double gfar,bool invert=false   )throw(cv::Exception);

	/**Adjust the parameters to the size of the image indicated
     */
    void resize(cv::Size size)throw(cv::Exception);

private:
	//GL routines

	static void argConvGLcpara2( double cparam[3][4], int width, int height, double gnear, double gfar, double m[16], bool invert )throw(cv::Exception);
	static int  arParamDecompMat( double source[3][4], double cpara[3][4], double trans[3][4] )throw(cv::Exception);
	static double norm( double a, double b, double c );
	static double dot(  double a1, double a2, double a3,
		double b1, double b2, double b3 );
};

