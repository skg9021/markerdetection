
#include "MarkerDetect.h"
#include "CameraParams.h"
#include "MarkerDraw.h"
#include <iostream>

int main(int, const char **)
{
	MarkerDetect mMarkerDetect;
	mMarkerDetect.help();
	CameraParams mCameraParameters;
	MarkerDraw mMarkerDraw;
	string TheIntrinsicFile = "C:\\opencvnew\\camera.yml";
	float mMarkerSize = 150;

	namedWindow( mMarkerDetect.wndname, 1 );
	namedWindow( mMarkerDetect.canny, 1 );
	namedWindow( "3D", 1 );

	vector<Marker> mMarker;
    vector<vector<Point> > squares;
    Mat image;
    Mat dst;
    
    VideoCapture cap(0);
	//read camera parameters if passed
	if (TheIntrinsicFile!="") {
		mCameraParameters.readFromXMLFile(TheIntrinsicFile);
		mCameraParameters.resize(Size(cap.get(CV_CAP_PROP_FRAME_WIDTH),cap.get(CV_CAP_PROP_FRAME_HEIGHT)));
	}


    while (true) {
        cap >> image;
        pyrDown( image, dst, Size( image.cols/2, image.rows/2 ) );
		mMarkerDetect.findSquares(dst, mMarker,mCameraParameters, mMarkerSize);
        mMarkerDetect.drawSquares(dst, mMarker);
        
		//draw a 3d cube in each marker if there is 3d info
		if (  mCameraParameters.isValid())
			for (unsigned int i=0;i<mMarker.size();i++) {
				//MarkerDraw::draw3dCube(dst,mMarker[i],mCameraParameters);
				MarkerDraw::draw3dAxis(dst,mMarker[i],mCameraParameters);
			}
		
		imshow( "3D", dst );
        int c = waitKey(5);
        if( (char)c == 27 )
            break;
    }
    
    return 0;

	
}