#pragma once

#include <opencv2\core\core.hpp>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui.hpp>

#include <iostream>
#include <math.h>
#include <string.h>
#include <valarray>

#include "Marker.h"
#include "CameraParams.h"

using namespace cv;
using namespace std;

class MarkerDetect
{
public:
	MarkerDetect(void);
	~MarkerDetect(void);

	void help();
	/**Detects the markers in the image passed
     *
     * If you provide information about the camera parameters and the size of the marker, then, the extrinsics of the markers are detected
     *
     * @param input input color image
     * @param detectedMarkers output vector with the markers detected
     * @param camParams Camera parameters
     * @param markerSizeMeters size of the marker sides expressed in meters
     * @param setYPerperdicular If set the Y axis will be perpendicular to the surface. Otherwise, it will be the Z axis
     */
    void findSquares(const cv::Mat &input,std::vector<Marker> &detectedMarkers, CameraParams camParams,float markerSizeMeters=-1,bool setYPerperdicular=true) throw (cv::Exception);

	void findSquares( const Mat& image, vector<Marker>& squares,Mat camMatrix ,Mat distCoeff ,float markerSizeMeters ,bool setYPerperdicular );
	void drawSquares( Mat& image, const vector<Marker>& squares );

	const char* wndname;
	const char* canny;

	vector<Marker> marker;
	
private:
	double angle( Point pt1, Point pt2, Point pt0 );
	/**
     */
    int perimeter(std::vector<cv::Point> &a);
	int		thresh, N;
	vector<vector<Point> > hull;
	
};

